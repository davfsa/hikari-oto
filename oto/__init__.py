# -*- coding: utf-8 -*-
# Copyright © Nekoka.tt 2020
#
# This file is part of Oto.
#
# Oto is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Oto is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Oto. If not, see <https://www.gnu.org/licenses/>.

from __future__ import annotations

from oto._about import __author__
from oto._about import __ci__
from oto._about import __copyright__
from oto._about import __discord_invite__
from oto._about import __docs__
from oto._about import __email__
from oto._about import __issue_tracker__
from oto._about import __license__
from oto._about import __url__
from oto._about import __version__
