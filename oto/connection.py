# -*- coding: utf-8 -*-
# Copyright © Nekoka.tt 2020
#
# This file is part of Oto.
#
# Oto is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Oto is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Oto. If not, see <https://www.gnu.org/licenses/>.
"""New module."""
from __future__ import annotations

__all__: typing.Final[typing.List[str]] = []

# noinspection PyUnresolvedReferences
import asyncio
import typing

from hikari.api import voice as voice_api
from hikari.events import voice as voice_events
from hikari.utilities import snowflake

from oto import gateway


Connection_T_inv = typing.TypeVar("Connection_T_inv", bound="Connection")
Connection_T_co = typing.TypeVar("Connection_T_co", bound="Connection", contravariant=True)


class Connection(voice_api.IVoiceConnection):
    @classmethod
    async def initialize(
        cls: typing.Type[Connection_T_co],
        channel_id: snowflake.Snowflake,
        debug: bool,
        endpoint: str,
        guild_id: snowflake.Snowflake,
        on_close: typing.Callable[[Connection_T_co], typing.Awaitable[None]],
        owner: voice_api.IVoiceComponent,
        session_id: str,
        shard_id: int,
        token: str,
        user_id: snowflake.Snowflake,
        **kwargs: typing.Any,
    ) -> Connection:
        conn = Connection(
            channel_id=channel_id,
            debug=debug,
            endpoint=endpoint,
            guild_id=guild_id,
            on_close=on_close,
            owner=owner,
            session_id=session_id,
            shard_id=shard_id,
            token=token,
            user_id=user_id,
        )
        await conn._connect()
        return conn

    def __init__(
        self,
        *,
        channel_id: snowflake.Snowflake,
        debug: bool,
        endpoint: str,
        guild_id: snowflake.Snowflake,
        on_close: typing.Callable[[Connection_T_co], typing.Awaitable[None]],
        owner: voice_api.IVoiceComponent,
        session_id: str,
        shard_id: int,
        token: str,
        user_id: snowflake.Snowflake,
    ) -> None:
        self._channel_id = channel_id
        self._debug = debug
        self._endpoint = endpoint
        self._guild_id = guild_id
        self._on_close = on_close
        self._owner = owner
        self._session_id = session_id
        self._shard_id = shard_id
        self._token = token
        self._user_id = user_id

        self._gateway = gateway.Gateway(
            debug=debug, endpoint=endpoint, guild_id=guild_id, session_id=session_id, token=token, user_id=user_id,
        )

        self._gateway_task: typing.Optional[asyncio.Task[None]] = None

    @property
    def channel_id(self) -> snowflake.Snowflake:
        return self._channel_id

    @property
    def guild_id(self) -> snowflake.Snowflake:
        return self._guild_id

    @property
    def is_alive(self) -> bool:
        return self._gateway.is_alive

    @property
    def owner(self) -> voice_api.IVoiceComponent:
        return self._owner

    @property
    def shard_id(self) -> int:
        return self._shard_id

    async def _connect(self) -> None:
        self._gateway_task = await self._gateway.start()
        self._gateway_task.add_done_callback(lambda _: self._disconnect_hook())

    async def disconnect(self) -> None:
        if self.is_alive is not None:
            self._disconnect_hook()
            await self._gateway.close()

    def _disconnect_hook(self):
        asyncio.create_task(
            self._on_close(self), name=f"invoke close callback for connection {self._guild_id}:{self._channel_id}"
        )

    async def join(self) -> None:
        if self._gateway_task:
            await self._gateway_task

    async def notify(self, event: voice_events.VoiceEvent) -> None:
        pass
