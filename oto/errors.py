# -*- coding: utf-8 -*-
# Copyright © Nekoka.tt 2020
#
# This file is part of Oto.
#
# Oto is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Oto is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Oto. If not, see <https://www.gnu.org/licenses/>.
"""New module."""
from __future__ import annotations

__all__: typing.Final[typing.List[str]] = [
    "OtoError",
    "VoiceGatewayError",
    "MediationAction",
    "VoiceGatewayClosedError",
]

# noinspection PyUnresolvedReferences
import enum
import typing


class OtoError(RuntimeError):
    __slots__ = ()

    def __repr__(self) -> str:
        return f"{type(self).__name__}({str(self)!r})"


class VoiceGatewayError(OtoError):
    """A base exception for a generic voice gateway error.

    Parameters
    ----------
    reason : builtins.str
        A string explaining the issue.
    """

    __slots__: typing.Sequence[str] = ("reason",)

    def __init__(self, reason: str) -> None:
        self.reason = reason

    def __str__(self) -> str:
        return f"Voice Gateway encountered an issue: {self.reason}"


@typing.final
@enum.unique
class MediationAction(enum.Enum):
    """A mediation action for how to respond to a voice gateway closing."""

    DIE = enum.auto()
    """Do not attempt to retry, just error."""

    IDENTIFY = enum.auto()
    """Attempt to start a new session."""

    RESUME = enum.auto()
    """Attempt to resume the existing session."""


class VoiceGatewayClosedError(VoiceGatewayError):
    """An exception raised when the server closes the connection.

    Parameters
    ----------
    reason : builtins.str
        A string explaining the issue.
    code : builtins.int or builtins.None
        The close code.
    mediation_action : MediationAction
        A mediation action to perform in response to this error.
    """

    __slots__: typing.Sequence[str] = ("code", "reason", "mediation_action")

    def __init__(self, reason: str, code: typing.Optional[int], mediation_action: MediationAction) -> None:
        super().__init__(reason)
        self.code = code
        self.mediation_action = mediation_action

    def __str__(self) -> str:
        return f"Server closed connection with code {self.code} because {self.reason}"
