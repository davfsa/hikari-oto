# -*- coding: utf-8 -*-
# Copyright © Nekoka.tt 2020
#
# This file is part of Oto.
#
# Oto is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Oto is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Oto. If not, see <https://www.gnu.org/licenses/>.
"""New module."""
from __future__ import annotations

__all__: typing.Final[typing.List[str]] = []

# noinspection PyUnresolvedReferences
import asyncio
import enum
import json
import logging
import math
import time
import typing

import aiohttp
import attr

import hikari
from hikari.impl import rate_limits

from oto import errors


class Gateway:
    @enum.unique
    @typing.final
    class _CloseCode(enum.IntEnum):
        RFC_6455_NORMAL_CLOSURE = 1000
        RFC_6455_GOING_AWAY = 1001
        RFC_6455_PROTOCOL_ERROR = 1002
        RFC_6455_TYPE_ERROR = 1003
        RFC_6455_ENCODING_ERROR = 1007
        RFC_6455_POLICY_VIOLATION = 1008
        RFC_6455_TOO_BIG = 1009
        RFC_6455_UNEXPECTED_CONDITION = 1011

        UNKNOWN_OPCODE = 4001
        ERROR_DECODING_PAYLOAD = 4002  # undocumented
        NOT_AUTHENTICATED = 4003
        AUTHENTICATION_FAILED = 4004
        ALREADY_AUTHENTICATED = 4005
        SESSION_NO_LONGER_VALID = 4006
        SESSION_TIMEOUT = 4009
        SERVER_NOT_FOUND = 4011
        UNKNOWN_PROTOCOL = 4012
        DISCONNECTED = 4014
        VOICE_SERVER_CRASHED = 4015
        UNKNOWN_ENCRYPTION_MODE = 4016

    @enum.unique
    @typing.final
    class _Opcode(enum.IntEnum):
        IDENTIFY = 0
        SELECT_PROTOCOL = 1
        READY = 2
        HEARTBEAT = 3
        SESSION_DESCRIPTION = 4
        SPEAKING = 5
        HEARTBEAT_ACK = 6
        RESUME = 7
        HELLO = 8
        RESUMED = 9
        CLIENT_DISCONNECT = 13

    class _SocketClosed(RuntimeError):
        __slots__ = ()

    VERSION: typing.Final[typing.ClassVar[int]] = 4
    _RESTART_RATELIMIT_WINDOW: typing.Final[typing.ClassVar[float]] = 30.0

    def __init__(
        self,
        *,
        debug: bool = False,
        endpoint: str,
        guild_id: hikari.Snowflake,
        http_settings: typing.Optional[hikari.HTTPSettings] = None,
        proxy_settings: typing.Optional[hikari.ProxySettings] = None,
        session_id: str,
        token: str,
        user_id: hikari.Snowflake,
    ) -> None:
        self._backoff = rate_limits.ExponentialBackOff(base=1.85, maximum=600, initial_increment=2)
        self._connected_at = float("nan")
        self._debug = debug
        self._endpoint = f"{endpoint}?v={self.VERSION}"
        self._guild_id = guild_id
        self._heartbeat_interval = float("nan")
        self._heartbeat_latency = float("nan")
        self._http_settings = http_settings
        self._last_heartbeat_sent = float("nan")
        self._last_message_received = float("nan")
        self._last_run_started_at = float("nan")
        self._logger = logging.getLogger(f"oto.gateway.{guild_id}")
        self._proxy_settings = proxy_settings
        self._nonce: typing.Optional[str] = None
        self._ready_event = asyncio.Event()
        self._request_close_event = asyncio.Event()
        self._run_task: typing.Optional[asyncio.Task[None]] = None
        self._session_id = session_id
        self._token = token
        self._user_id = user_id
        self._ws: typing.Optional[aiohttp.ClientWebSocketResponse] = None
        self._zombied = False

        # Stuff we get from the READY payload
        self._udp_ip: typing.Optional[str] = None
        self._udp_port: typing.Optional[int] = None
        self._udp_ssrc: typing.Optional[str] = None
        self._udp_modes: typing.Optional[typing.Sequence[str]] = None

    @property
    @typing.final
    def is_alive(self) -> bool:
        return not math.isnan(self._connected_at)

    @property
    @typing.final
    def heartbeat_latency(self) -> float:
        return self._heartbeat_latency

    async def start(self) -> asyncio.Task[None]:
        self._run_task = asyncio.create_task(self._run(), name=f"voice {self._user_id}:{self._guild_id} keep-alive")
        await self._ready_event.wait()
        if self._run_task.done():
            # Propage any errors that may occur...
            await self._run_task

        return self._run_task

    async def close(self) -> None:
        if not self._request_close_event.is_set() and self._run_task is not None and not self._run_task.done():
            self._request_close_event.set()

    async def _run(self) -> None:
        # aiohttp.ClientTimeout is frozen.
        if self._http_settings is not None:
            timeout = aiohttp.ClientTimeout(
                total=self._http_settings.timeouts.total,
                connect=self._http_settings.timeouts.acquire_and_connect,
                sock_read=self._http_settings.timeouts.request_socket_read,
                sock_connect=self._http_settings.timeouts.request_socket_connect,
            )
        else:
            timeout = aiohttp.ClientTimeout()

        async with aiohttp.ClientSession(
            connector_owner=True,
            connector=aiohttp.TCPConnector(
                verify_ssl=self._http_settings.verify_ssl if self._http_settings is not None else True,
                # We are never going to want more than one connection. This will be spammy on
                # big sharded bots and waste a lot of time, so theres no reason to bother.
                limit=1,
                limit_per_host=1,
            ),
            version=aiohttp.HttpVersion11,
            timeout=timeout,
            trust_env=self._proxy_settings.trust_env if self._proxy_settings is not None else False,
        ) as client_session:
            try:
                # This may be set if we are stuck in a reconnect loop.
                while not self._request_close_event.is_set() and await self._run_once_shielded(client_session):
                    pass
            finally:
                await self.close()
                # This is set to ensure that the `start' waiter does not deadlock if
                # we cannot connect successfully. It is a hack, but it works.
                self._ready_event.set()

    async def _run_once_shielded(self, client_session: aiohttp.ClientSession) -> bool:
        try:
            await self._run_once(client_session)
            return False
        except aiohttp.ClientConnectorError as ex:
            self._logger.error(
                "failed to connect to Discord because %s.%s: %s", type(ex).__module__, type(ex).__qualname__, str(ex),
            )
        except self._SocketClosed:
            # The socket has already closed, so no need to close it again.
            if self._zombied:
                self._backoff.reset()

            if not self._request_close_event.is_set():
                self._logger.warning("unexpected socket closure, will attempt to resume")

            return not self._request_close_event.is_set()

        except errors.VoiceGatewayClosedError as ex:
            if ex.mediation_action == errors.MediationAction.RESUME:
                self._logger.warning(
                    "server closed the connection with %s (%s), will attempt to resume", ex.code, ex.reason,
                )

            elif ex.mediation_action == errors.MediationAction.IDENTIFY:
                self._logger.warning(
                    "server closed the connection with %s (%s), will attempt to reconnect", ex.code, ex.reason,
                )
                self._nonce = None
                self._session_id = None

            else:
                self._logger.warning(
                    "server closed the connection with %s (%s), giving up", ex.code, ex.reason,
                )
                self._nonce = None
                self._session_id = None
                raise

        except Exception as ex:
            self._logger.error("unexpected exception occurred, shard will now die", exc_info=ex)
            self._nonce = None
            self._session_id = None
            await self._close_ws(
                self._CloseCode.RFC_6455_UNEXPECTED_CONDITION, "unexpected error occurred",
            )
            raise

    async def _run_once(self, client_session: aiohttp.ClientSession) -> None:
        self._request_close_event.clear()
        self._zombied = False

        if self._now() - self._last_run_started_at < self._RESTART_RATELIMIT_WINDOW:
            # Interrupt sleep immediately if a request to close is fired.
            wait_task = asyncio.create_task(
                self._request_close_event.wait(), name=f"voice websocket {self._user_id}:{self._guild_id} backing off",
            )
            try:
                sleep_for = next(self._backoff)
                self._logger.debug("backing off for %ss", sleep_for)
                await asyncio.wait_for(wait_task, timeout=sleep_for)

                # If this line gets reached, the wait didn't time out, meaning
                # the user told the client to shut down gracefully before the
                # backoff completed.
                return
            except asyncio.TimeoutError:
                pass

        # Do this after. It prevents backing off on the first try.
        self._last_run_started_at = self._now()

        self._logger.debug("creating websocket connection to %s", self._endpoint)

        self._ws = await client_session.ws_connect(
            url=self._endpoint,
            autoping=True,
            autoclose=True,
            proxy=self._proxy_settings.url if self._proxy_settings is not None else None,
            proxy_headers=self._proxy_settings.all_headers if self._proxy_settings is not None else None,
            verify_ssl=self._http_settings.verify_ssl if self._http_settings is not None else None,
            # TODO: max message size?
        )

        self.connected_at = self._now()

        try:
            self._ready_event.clear()
            self._request_close_event.clear()

            await self._handshake()

            # We should ideally set this after HELLO, but it should be fine
            # here as well. If we don't heartbeat in time, something probably
            # went majorly wrong anyway.
            heartbeat = asyncio.create_task(
                self._heartbeat_keepalive(), name=f"voice websocket {self._user_id}:{self._guild_id} heartbeat",
            )

            try:
                await self._poll_events()
            finally:
                heartbeat.cancel()

        finally:
            self._connected_at = float("nan")

    async def _handshake(self) -> None:
        data = await self._expect_opcode(self._Opcode.HELLO)
        self._heartbeat_interval = float(data["heartbeat_interval"]) / 1_000
        self._logger.debug("heartbeat interval is %ss", self._heartbeat_interval)

        if self._nonce is None:
            self._logger.debug("sending IDENTIFY opcode to session %s", self._session_id)
            await self._send_json(
                {
                    "op": self._Opcode.IDENTIFY,
                    "d": {
                        "server_id": str(self._guild_id),
                        "user_id": str(self._user_id),
                        "session_id": self._session_id,
                        "token": self._token,
                    },
                }
            )

            payload = await self._expect_opcode(self._Opcode.IDENTIFY)
            self._udp_ip = payload["ip"]
            self._udp_port = payload["port"]
            self._udp_modes = payload["modes"]
            self._udp_ssrc = payload["ssrc"]

            self._logger.info(
                "received READY opcode (ip:%s, port:%s, modes:%s, ssrc:%s)",
                self._udp_ip,
                self._udp_port,
                self._udp_modes,
                self._udp_ssrc,
            )

        else:
            self._logger.debug("sending RESUME opcode to session %s with nonce %s", self._session_id, self._nonce)
            await self._send_json(
                {
                    "op": self._Opcode.RESUME,
                    "d": {"server_id": str(self._guild_id), "session_id": self._session_id, "token": self._token},
                }
            )
            await self._expect_opcode(self._Opcode.RESUMED)
            self._logger.info("received RESUMED opcode")

    async def _heartbeat_keepalive(self) -> None:
        self._logger.info("in heartbeat_keepalive")

    async def _poll_events(self) -> None:
        pass

    async def _close_ws(self, code: int, message: str) -> None:
        self._logger.debug("sending close frame with code %s and message %r", int(code), message)
        # None if the websocket encountered error'ed on initialization.
        if self._ws is not None:
            await self._ws.close(code=code, message=bytes(message, "utf-8"))

    async def _expect_opcode(self, opcode: _Opcode) -> typing.Mapping[str, typing.Any]:
        message = await self._receive_json()
        op = message["op"]

        if op == opcode:
            return message["d"]

        error_message = f"Unexpected opcode {op} received, expected {opcode}"
        await self._close_ws(self._CloseCode.RFC_6455_PROTOCOL_ERROR, error_message)
        raise errors.VoiceGatewayError(error_message)

    async def _send_json(self, payload: typing.Mapping[str, typing.Any]) -> None:
        message = json.dumps(payload)
        self._log_debug_payload(message, "sending json payload [op:%s]", int(payload.get("op")))
        await self._ws.send_str(message)  # type: ignore[union-attr]

    async def _receive_json(self) -> typing.Mapping[str, typing.Any]:
        message = await self._ws.receive()
        self._last_message_received = self._now()

        payload: typing.Mapping[str, typing.Any]

        if message.type == aiohttp.WSMsgType.TEXT:
            string = message.data
            payload = json.loads(string)  # type: ignore[assignment]
            self._log_debug_payload(string, "received text payload [op:%s]", payload.get("op"))
            return payload

        if message.type == aiohttp.WSMsgType.CLOSE:
            close_code = self._ws.close_code  # type: ignore[union-attr]
            reason = message.extra
            self._logger.debug("connection closed with code %s (%s)", close_code, reason)

            if close_code == self._CloseCode.VOICE_SERVER_CRASHED:
                mediation_action = errors.MediationAction.RESUME
            elif close_code < 4000 or close_code in (
                self._CloseCode.SESSION_NO_LONGER_VALID,
                self._CloseCode.SESSION_TIMEOUT,
            ):
                mediation_action = errors.MediationAction.IDENTIFY
            else:
                mediation_action = errors.MediationAction.DIE

            raise errors.VoiceGatewayClosedError(reason=reason, code=close_code, mediation_action=mediation_action)

        if message.type == aiohttp.WSMsgType.CLOSING or message.type == aiohttp.WSMsgType.CLOSED:
            raise self._SocketClosed

        if message.type == aiohttp.WSMsgType.BINARY:
            self._logger.critical(
                "Discord sent us garbage binary data. This is probably a Discord issue."
            )

            await self._close_ws(
                code=self._CloseCode.RFC_6455_PROTOCOL_ERROR,
                message="You sent data in an unrecognised format",
            )

            raise errors.VoiceGatewayError("Discord sent us garbage.")

        if message.type == aiohttp.WSMsgType.ERROR:
            # Assume exception for now.
            ex = self._ws.exception()  # type: ignore[union-attr]
            self._logger.warning(
                "encountered unexpected error: %s",
                ex,
                exc_info=ex if self._logger.isEnabledFor(logging.DEBUG) else None,
            )
            raise errors.VoiceGatewayError("Unexpected websocket exception from gateway") from ex

        # This should NEVER occur unless we broke this badly.
        raise TypeError("Unexpected message type " + message.type)

    @staticmethod
    def _now() -> float:
        return time.perf_counter()

    def _log_debug_payload(self, payload: str, message: str, *args: typing.Any) -> None:
        # Prevent logging these payloads if logging isn't enabled. This aids performance a little.
        if not self._logger.isEnabledFor(logging.DEBUG):
            return

        message = f"{message} [nonce:%s, session_id:%s, endpoint:%s, size:%s]"
        if self._debug:
            message = f"{message} with raw payload: %s"
            args = (
                *args,
                self._nonce,
                self._session_id,
                self._endpoint,
                len(payload),
                payload,
            )
        else:
            args = (*args, self._nonce, self._session_id, self._endpoint, len(payload))

        self._logger.debug(message, *args)
